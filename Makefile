# Este es el archivo Makefile de la práctica, editarlo como sea neceario
# Target que se ejecutará por defecto con el comando make
all:	dynamic
	
	

# Target que compilará el proyecto usando librerías estáticas
static:
	
	gcc -c src/main.c
	gcc -c lib/vector.c
	ar rcs libvector.a vector.o
	gcc -static -o mainstatic main.o -L. -lvector -lm




# Target que compilará el proyecto usando librerías dinámicas
dynamic:
	gcc -shared -fPIC -o libvector.so lib/vector.c
	gcc -o maindynamic src/main.c ./libvector.so -lm




# Limpiar archivos temporales
clean:
	rm -f *.o *.a *.so main*
