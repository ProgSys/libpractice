#include <stdio.h>
#include "../include/vector.h"

int main()
{
	float x;
	struct vector3D vector;
	struct vector3D p1, p2;
	int result;
	printf("\nIProducto Punto:\n");
	printf("\nIngrese las coordenadas del vector 1:\n");
	printf("Coordenada x1: \t");
	scanf("%f",&x);
	p1.x = x;
	printf("Coordenada y1: \t");
	scanf("%f",&x);
	p1.y = x;
	printf("Coordenada z1: \t");
	scanf("%f",&x);
	p1.z = x;
	printf("\nIngrese las coordenadas del vector 2:\n");
	printf("Coordenada x2: \t");
	scanf("%f",&x);
	p2.x = x;
	printf("Coordenada y2: \t");
	scanf("%f",&x);
	p2.y = x;
	printf("Coordenada z2: \t");
	scanf("%f",&x);
	p2.z = x;
	x = dotproduct(p1,p2);
	printf("\nEl producto punto es: \t%f \n",x);
	vector = crossproduct(p1,p2);
	printf("\nEl vector resultante del producto cruz es: \n\t [%f, %f, %f] \n",vector.x, vector.y, vector.z);
	x = magnitud(p1);
	printf("\nLa magnitud del vector 1 es: \t%f",x);
	x = magnitud(p2);
	printf("\nLa magnitud del vector 2 es: \t%f",x);
	result = esOrtogonal(p1,p2);
	if (result == 1){
		printf("\nLos vectores son ortogonales." );
	}else{
		printf("\nLos vectores no son ortogonales.");
	};
	return 0;
}