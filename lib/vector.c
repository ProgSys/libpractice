/* implementar aquí las funciones requeridas */
#include <math.h>
#include "../include/vector.h"

float dotproduct(vector3D a, vector3D b)
{
 	float result = a.x*b.x + a.y*b.y + a.z*b.z;
 	return result;
};

vector3D crossproduct(vector3D v1, vector3D v2)
{
	vector3D result;
	result.x = (v1.y * v2.z) - (v1.z * v2.y);
	result.y = - ((v1.z * v2.x) - (v1.x * v2.z));
	result.z = (v1.x * v2.y) - (v1.y * v2.x);
	return result;
};

float magnitud(vector3D a)
{
	float result;
	result = pow(a.x,2) + pow(a.y,2) + pow(a.z,2);
	result = pow(result, 0.5);
	return result; 
};

int esOrtogonal(vector3D a, vector3D b)
{
	float dp = dotproduct(a, b);
	if (dp == 0)
	{
		return 1;
	}
	else 
	{
		return 0;
	}

};