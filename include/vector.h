/* Definicion del tipo de datos vector3D */
typedef struct vector3D{
	float x;
	float y;
	float z;
} vector3D;

float dotproduct(vector3D, vector3D);

vector3D crossproduct(vector3D, vector3D);

float magnitud(vector3D);

int esOrtogonal(vector3D, vector3D);
